
var imageArea = q(".js-image-area");
var imageName = q(".js-image-name");

function uploadFile(event) {
    var file = event.target.files[0];

    if (file == null) {
        return;
    }

    event.target.value = null;

    showInfo("Uploading...");

    var formData = new FormData();
    formData.append("file", file);

    myFetch(getEndpointUrl("images"), {
        method: "POST",
        body: formData,
        mode: "cors",
    }).then(() => {
        showSuccess("Upload Complete!");
    });
}

function findImage() {
    const name = imageName.value.trim();
    if (!name) {
        showError("Image name is empty. Please fix", 3000);
        return;
    }

    findImageByUrl(getEndpointUrl("images/" + name));
}

function findRandomImage() {
    findImageByUrl(getEndpointUrl("images/random"));
}

function findImageByUrl(url) {
    imageArea.innerHTML = "";

    showInfo("Load Image...");

    var image = new Image();
    image.src = url;
    image.onload = function () {
        imageArea.append(image);
        showSuccess("Image Loaded!");
    };
    image.onerror = function () {
        showError("Image cannot be loaded. Look at console to get more details");
    }
}
