
var subscriberList = q(".js-subscriber-list");
var subscriberEmail = q(".js-subscriber-email");

function createSubscriber() {
    const email = subscriberEmail.value.trim();
    if (!email) {
        showError("Email cannot not be empty.", 3000);
        return;
    }
    if (!/[a-zA-Z0-9.\-]+@[a-zA-Z0-9\-]+(\.[a-zA-Z0-9.]+)+/.test(email)) {
        showError("It seems value your entered is not email");
        return;
    }

    showInfo("Creating new subscriber (" + email + ")...");

    myFetch(getEndpointUrl("subscribers"), {
        method: "POST",
        mode: "cors",
        body: JSON.stringify({ email: email }),
        headers: {
            "Content-Type": "application/json"
        }
    }).then(() => loadSubscribers());
}

function deleteSubscriber(email) {
    showInfo("Deleting subscriber (" + email + ")...");

    myFetch(getEndpointUrl("subscribers/" + email), {
        method: "DELETE",
        mode: "cors",
    }).then(() => loadSubscribers());
}

function loadSubscribers() {
    showInfo("Loading...");

    return myFetch(getEndpointUrl("subscribers"), {
        method: "GET",
        mode: "cors",
    })
        .then((response) => response.json())
        .then((subscribers) => {
            subscriberList.innerHTML = subscribers.map(function(email) {
                return ["<li>", email, " <button type='button' onclick=\"deleteSubscriber('", email, "')\">Delete</button></li>"].join("");
            }).join("");
            showSuccess("Subscribers were loaded!");
        });
}

loadSubscribers();
