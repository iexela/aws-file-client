const apiUrl = "http://54.188.188.151:8080";

function q(selector) {
    return document.querySelector(selector);
}

var notificationArea = q(".js-notification-area");

function getEndpointUrl(path) {
    return apiUrl + "/" + (path || "");
}

function showError(text, delay) {
    showNotification(text, "error", delay);
}

function showSuccess(text, delay) {
    showNotification(text, "success", delay);
}

function showInfo(text, delay) {
    showNotification(text, "info", delay);
}

function clearNotificationArea() {
    showNotification("");
}

var hideTimeout;

function showNotification(text, severity, delay) {
    clearTimeout(hideTimeout);

    notificationArea.classList.remove("notification-area--success");
    notificationArea.classList.remove("notification-area--error");
    notificationArea.classList.remove("notification-area--info");

    notificationArea.classList.add("notification-area--" + severity);
    notificationArea.textContent = text;

    if (delay) {
        setTimeout(function() {
            clearNotificationArea();
        }, delay);
    }
}

function myFetch(url, options) {
    return fetch(url, options)
        .catch((error) => {
            showError("Server Error: " + error.message);
            return Promise.reject();
        })
        .then((response) => {
            if (response.ok) {
                return Promise.resolve(response);
            } else {
                showError("Server responded with status: " + response.status + " " + response.statusText);
                return Promise.reject();
            }
        });

}
